﻿
using DotNetty.Codecs;
using DotNetty.Handlers.Logging;
using DotNetty.Handlers.Tls;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Groups;
using DotNetty.Transport.Channels.Sockets;
using SF2_emu.PacketHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SF2_emu
{
    class Program
    {
        public static int sessionCount = 0;
        public static bool connected = false;
        public static List<IChannel> channels = new List<IChannel>();
        public static List<KeyValuePair<int, AbstractHandler>> handlers = new List<KeyValuePair<int, AbstractHandler>>();


        static async Task RunServerAsync()
        {

            var bossGroup = new MultithreadEventLoopGroup(1);
            var workerGroup = new MultithreadEventLoopGroup();

            var encoder = new ByteEncoder();
            var decoder = new ByteDecoder();

            handlers.Add(new KeyValuePair<int, AbstractHandler>(0x65, new Messages.LoginMessage()));

            try
            {
                var bootstrap = new ServerBootstrap();
                bootstrap
                    .Group(bossGroup, workerGroup)
                    .Channel<TcpServerSocketChannel>()
                    .Option(ChannelOption.SoBacklog, 100)
                    .Handler(new LoggingHandler(LogLevel.TRACE))
                    .ChildHandler(new ActionChannelInitializer<ISocketChannel>(channel =>
                    {
                        IChannelPipeline pipeline = channel.Pipeline;
                        pipeline.AddLast(decoder,  encoder, new PacketReceiver());

                    }));

                IChannel bootstrapChannel = await bootstrap.BindAsync(27932);

                Console.WriteLine("Server is waiting for connection");

                while (Console.ReadKey().Key != ConsoleKey.Q ) {
                    foreach(var channel in channels) {

                        Console.WriteLine(channel.RemoteAddress.ToString());
                    }
                }

                await bootstrapChannel.CloseAsync();
            }
            finally
            {
                Task.WaitAll(bossGroup.ShutdownGracefullyAsync(), workerGroup.ShutdownGracefullyAsync());
            }
        }

        static void Main() => RunServerAsync().Wait();
    }
}
