﻿using DotNetty.Transport.Channels;
using SF2_emu.Network;

namespace SF2_emu.PacketHandlers
{
    public abstract class AbstractHandler
    {
        public abstract void HandleMessage(IChannelHandlerContext context, Packet packet);
    }
}