﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF2_emu
{
    class ByteDecoder : ByteToMessageDecoder
    {

        protected override void Decode(IChannelHandlerContext context, IByteBuffer input, List<object> output)
        {
            IByteBuffer buffer = input as IByteBuffer;

            byte[] extracted = new byte[buffer.ReadableBytes];
            buffer.ReadBytes(extracted, 0, buffer.ReadableBytes);

            output.Add(extracted);

            Console.WriteLine("Decoded");
        }
    }
}
