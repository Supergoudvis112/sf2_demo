﻿using System;
using System.Collections.Generic;
using DotNetty.Transport.Channels;
using DotNetty.Codecs;
using DotNetty.Buffers;

namespace SF2_emu
{
    class ByteEncoder : MessageToMessageEncoder<byte[]>
    {
        protected override void Encode(IChannelHandlerContext context, byte[] message, List<object> output)
        {
            IByteBuffer buffer = Unpooled.Buffer();
            buffer.WriteBytes(message);
            
            output.Add(buffer);

            Console.WriteLine("Encoded");
        }
    }
     
}
