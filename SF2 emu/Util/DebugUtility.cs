﻿using DotNetty.Buffers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SF2_emu
{
    public class DebugUtility
    {
        //=============================================================================================
        /// <summary>
        /// Creates hex dump of byte array
        /// </summary>
        /// <param name="buffer">Byte array</param>
        /// <returns>Formatted byte dump</returns>
        ///
        public static string HexDump(byte[] buffer)
        {
            return HexDump(buffer, 0, buffer.Length);
        }
        //=============================================================================================
        /// <summary>
        /// Creates hex dump of byte array
        /// </summary>
        /// <param name="buffer">Byte array</param>
        /// <param name="offset">Offset in byte array to start with</param>
        /// <param name="count">Count of bytes to dump (starting from offset)</param>
        /// <returns></returns>
        public static string HexDump(byte[] buffer, int offset, int count)
        {
            const int bytesPerLine = 16;
            StringBuilder output = new StringBuilder();
            StringBuilder ascii_output = new StringBuilder();
            int length = count;
            if (length % bytesPerLine != 0)
            {
                length += bytesPerLine - length % bytesPerLine;
            }
            for (int x = 0; x <= length; ++x)
            {
                if (x % bytesPerLine == 0)
                {
                    if (x > 0)
                    {
                        output.AppendFormat("  {0}{1}", ascii_output.ToString(), Environment.NewLine);
                        ascii_output.Clear();
                    }
                    if (x != length)
                    {
                        output.AppendFormat("{0:d10}   ", x);
                    }
                }
                if (x < count)
                {
                    output.AppendFormat("{0:X2} ", buffer[offset + x]);
                    char ch = (char)buffer[offset + x];
                    if (!Char.IsControl(ch))
                    {
                        ascii_output.AppendFormat("{0}", ch);
                    }
                    else
                    {
                        ascii_output.Append(".");
                    }
                }
                else
                {
                    output.Append("   ");
                    ascii_output.Append(".");
                }
            }
            return output.ToString();
        }

        public static byte[] ObjectToByteArray(object unknownObject)
        {
            //No need to go trough an empty object.
            if (unknownObject == null)
            {
                return null;
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, unknownObject);
                return memoryStream.ToArray();
            }
        }

        //=============================================================================================
        /// <summary>
        /// Gets basic information about caller method
        /// </summary>
        /// <returns></returns>
        public static string GetCallerMethodInfo()
        {  
            StackTrace trace = new StackTrace();
            StackFrame frame = trace.GetFrame(2);

            MethodBase method = frame.GetMethod();
            string ret = string.Format("Location [{0}] MethodName [{1}]", method.ReflectedType.FullName, method.Name);

            return ret;
        }
        //=============================================================================================

    }
}
