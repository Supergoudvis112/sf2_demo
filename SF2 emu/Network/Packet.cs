﻿using DotNetty.Buffers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF2_emu.Network
{
    public class Packet
    {
        public byte[] Data { get;}
        public PacketSource packetSource { get; }
        public Int16 Opcode { get; }
        public Int16 PacketType { get; }
        public Int16 Size
        {
            get
            {
                 Int16 length = packetSource == PacketSource.Client ? (Int16)4 : (Int16)6;
                 length += (Int16)Data.Length;
                 return length; 
            }
        }

        private MemoryStream _stream;
        private BinaryReader _reader;
        private BinaryWriter _writer;

        public Packet(Int16 Opcode, PacketSource packetSource)
        {
            this.Opcode = Opcode;
            this.packetSource = packetSource;
        }

        public Packet(byte[] Completebuffer, PacketSource packetSource)
        {
            Console.WriteLine($"Length of buffer: {Completebuffer.Length}");
            //Split packet into two parts
            Data = Completebuffer.Skip(packetSource == PacketSource.Client ? 4 : 6).ToArray();
            Console.WriteLine(Data.Length);
            if (packetSource == PacketSource.Client)
            {
                Opcode = BitConverter.ToInt16(Completebuffer, 2);
            }
            else
            {
                PacketType = BitConverter.ToInt16(Completebuffer, 2);
                Opcode = BitConverter.ToInt16(Completebuffer, 4);
            }

            this.packetSource = packetSource;
        }

        public Packet(Int16 Opcode, Int16
            Packettype, PacketSource packetSource)
        {
            this.Opcode = Opcode;
            this.packetSource = packetSource;
            this.PacketType = Packettype;
        }

        public void SeekToBeginning()
        {
            _stream.Seek(0, SeekOrigin.Begin);
        }

        public byte[] getCompletePacket()
        {
            byte[] result = new byte[Data.Length + packetSource == PacketSource.Server ? 6 : 4];

            var length = BitConverter.GetBytes(Size);
            length.CopyTo(result, 0);

            if (packetSource == PacketSource.Server)
            {
                var packettype = BitConverter.GetBytes(PacketType);
                length.CopyTo(result, 2);

                var opcode = BitConverter.GetBytes(Opcode);
                opcode.CopyTo(result, 4);
            }
            else
            {
                var opcode = BitConverter.GetBytes(Opcode);
                opcode.CopyTo(result, 2);
            }

            Data.CopyTo(result, packetSource == PacketSource.Server ? 6 : 4);
            return result;
        }

        #region Reading

        //Single value reading
        public T ReadValue<T>()
        {
            T result = default(T);
            Type tType = typeof(T);

            bool readOk = false;

            //Everything OK
            if (tType == typeof(Boolean))
            {
                result = (T)Convert.ChangeType(_reader.ReadBoolean(), tType);
                readOk = true;
            }
            if (tType == typeof(UInt16))
            {
                result = (T)Convert.ChangeType(_reader.ReadUInt16(), tType);
                readOk = true;
            }

            if (tType == typeof(Int16))
            {
                result = (T)Convert.ChangeType(_reader.ReadInt16(), tType);
                readOk = true;
            }

            if (tType == typeof(UInt32))
            {
                result = (T)Convert.ChangeType(_reader.ReadUInt32(), tType);
                readOk = true;
            }

            if (tType == typeof(Int32))
            {
                result = (T)Convert.ChangeType(_reader.ReadInt32(), tType);
                readOk = true;
            }

            if (tType == typeof(Byte))
            {
                result = (T)Convert.ChangeType(_reader.ReadByte(), tType);
                readOk = true;
            }

            if (tType == typeof(SByte))
            {
                result = (T)Convert.ChangeType(_reader.ReadSByte(), tType);
                readOk = true;
            }

            if (!readOk)
                throw new ArgumentException(
                    string.Format("Reading type [{0}] is not supported by Packet class", tType.Name)
                    );

            return result;
        }

        //Array reading
        public List<T> ReadList<T>(int count)
        {
            List<T> result = new List<T>(count);
            for (int i = 0; i < count; i++)
                result.Add(ReadValue<T>());
            return result;
        }

        //Why to complicate stuff with generics ?
        public string ReadASCII_ByteLen()
        {
            Byte len = _reader.ReadByte();
            byte[] bytes = _reader.ReadBytes(len);
            return ASCIIEncoding.ASCII.GetString(bytes);
        }

        public string ReadASCII_ZeroTerminated()
        {
            byte[] buffer = new byte[128];
            byte tmp;
            int index = 0;
            while ((tmp = ReadValue<byte>()) != 0x0)
            {
                buffer[index++] = tmp;
            }

            return ASCIIEncoding.ASCII.GetString(buffer, 0, index);
        }
        #endregion

        #region Writing
        public void WriteValue<T>(T value)
        {
            Type tType = typeof(T);
            bool writeOk = false;

            if (tType == typeof(byte[]))
            {
                byte[] tmp = (byte[])Convert.ChangeType(value, tType);
                _writer.Write(Array.ConvertAll(tmp, b => (byte)b), 0, tmp.Length);
                writeOk = true;
            }

            //Everything OK
            if (tType == typeof(Boolean))
            {
                Boolean tmp = (Boolean)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(UInt16))
            {
                UInt16 tmp = (UInt16)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(Int16))
            {
                Int16 tmp = (Int16)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(UInt32))
            {
                UInt32 tmp = (UInt32)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(Int32))
            {
                Int32 tmp = (Int32)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(Byte))
            {
                Byte tmp = (Byte)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (tType == typeof(SByte))
            {
                SByte tmp = (SByte)Convert.ChangeType(value, tType);
                _writer.Write(tmp);
                writeOk = true;
            }

            if (!writeOk)
                throw new ArgumentException(
                    string.Format("Writing type [{0}] is not supported by Packet class", tType.Name)
                    );
        }


        public void WriteList<T>(List<T> values, int count)
        {
            for (int i = 0; i < count; i++)
            {
                WriteValue<T>(values[i]);
            }
        }

        public void WriteArray(byte[] values)
        {
            WriteValue<byte[]>(values);
        }

        public void WriteASCII_ByteLen(string str)
        {
            if (str.Length > Byte.MaxValue)
                throw new ArgumentException(
                    string.Format("ASCII string too long for Packet class [{0}]", str.Length)
                    );

            byte[] bytes = ASCIIEncoding.ASCII.GetBytes(str);
            WriteValue<Byte>((Byte)str.Length);
            WriteList<Byte>(bytes.ToList(), str.Length);
        }

        public void WriteASCII_ZeroTerminated(string str)
        {
            byte[] buffer = ASCIIEncoding.ASCII.GetBytes(str);
            WriteList<byte>(buffer.ToList(), buffer.Length);
            WriteValue<byte>(0x0);

        }

        public void WriteASCII(string str)
        {
            byte[] buffer = ASCIIEncoding.ASCII.GetBytes(str);
            WriteList<byte>(buffer.ToList(), buffer.Length);
        }

        #endregion
    }
}
