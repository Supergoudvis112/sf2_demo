﻿using DotNetty.Transport.Channels;
using SF2_emu.Network;
using SF2_emu.PacketHandlers;
using System;
using System.Linq;

namespace SF2_emu
{

    class PacketReceiver : ChannelHandlerAdapter
    {

        public override void ChannelUnregistered(IChannelHandlerContext context) {
            //We disconnected the client
            Program.channels.Remove(context.Channel);
        }

        public override void ChannelRegistered(IChannelHandlerContext context) {
            Program.channels.Add(context.Channel);

            byte[] acceptConnection = new byte[]{ 0x04, 0x00, 0xD0, 0x07 };
            context.WriteAndFlushAsync(acceptConnection);
        }

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            byte[] buffer = message as byte[];
            Packet packet = new Packet(buffer, PacketSource.Client);

            Console.WriteLine($"Buffersize: {buffer.Length} Packetsize: {packet.Size} Command: {packet.Opcode} Packettype: {packet.PacketType} PacketSource: {packet.packetSource}");
            Console.WriteLine(DebugUtility.HexDump(packet.Data));

            AbstractHandler handler = Program.handlers.SingleOrDefault(x => x.Key == packet.Opcode).Value;
            if (handler != null)
            {
                handler.HandleMessage(context, packet);
            }
            else {
                Console.WriteLine($"Packet {packet.Opcode} has no handler yet.");
            }
        }

        public override void ChannelReadComplete(IChannelHandlerContext context) => context.Flush();

        //Don't know when we will get an Exception, catch it to be sure tho
        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.WriteLine("Exception: " + exception);
            context.CloseAsync();
        }
    }
}
